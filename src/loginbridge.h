#ifndef LOGINBRIDGE_H
#define LOGINBRIDGE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <Sailfish/Secrets/secretmanager.h>
#include <Sailfish/Secrets/secret.h>
#include <Sailfish/Crypto/cryptomanager.h>
#include <Sailfish/Crypto/key.h>

using namespace Sailfish;

class LoginBridge : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString homeserverUrl READ homeserverUrl WRITE setHomeserverUrl )
    Q_PROPERTY(QString username READ username WRITE setUsername)
    Q_PROPERTY(QString password READ password WRITE setPassword)
    Q_PROPERTY(QString error READ error WRITE setError NOTIFY errorChanged)
public:
    Q_INVOKABLE void login(bool serverDiscoveryOnly = false);
    Q_INVOKABLE void ssoLogin(QString token);
    QString homeserverUrl();
    QString username();
    QString password();
    QString error();
    void setHomeserverUrl(const QString &hsUrl);
    void setUsername(const QString &username);
    void setPassword(const QString &password);
    void setError(const QString &error);
signals:
    void errorChanged();
    void discovered();
private slots:
    void doServerDiscovery(QNetworkReply* reply);
    void validateHomeserver(QNetworkReply* reply);
    void validateFlow(QNetworkReply* reply);
    void doLogin(QNetworkReply* reply);
    void finishUploadKeys(QNetworkReply* reply);

private:
    QString m_homeserverUrl;
    QString m_username;
    QString m_password;
    QString m_error;
    bool m_serverDiscoveryOnly = false;
    QNetworkAccessManager* manager;
    QNetworkAccessManager* keys_upload;
    Secrets::SecretManager m_secretManager;
    Crypto::CryptoManager m_cryptoManager;
};

#endif // LOGINBRIDGE_H

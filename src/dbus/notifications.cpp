#include <QJsonArray>
#include <QTimer>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QFileInfo>
#include <QStandardPaths>
#include <nemonotifications-qt5/notification.h>
#include <keepalive/backgroundactivity.h>

#include "notifications.h"
#include "sailtrixsignals.h"
#include "../enc-util.h"

Notifications::Notifications(QObject *parent, SailtrixSignals* _sig) : QObject(parent), manager { new QNetworkAccessManager(this) }, activity { new BackgroundActivity(this) }, sig { _sig }
{
    connect(activity, &BackgroundActivity::running, this, &Notifications::startRun);
    connect(manager, &QNetworkAccessManager::finished, this, &Notifications::processNotifications);
    connect(sig, &SailtrixSignals::notificationsDisabled, this, &Notifications::disable);
    connect(sig, &SailtrixSignals::notificationsEnabled, this, &Notifications::enable);
    connect(sig, &SailtrixSignals::notificationIntervalChanged, this, &Notifications::changeFrequency);

    start_time = QDateTime::currentMSecsSinceEpoch();
    activity->setWakeupFrequency(freq);
    activity->wait(freq);
}

Notifications::~Notifications() {
    delete manager;
    delete activity;
}

void Notifications::changeFrequency(int n) {
    if (activity->isWaiting()) {
        activity->stop();
    }

    switch (n) {
    case 0:
        freq = BackgroundActivity::ThirtySeconds;
        break;
    case 1:
        freq = BackgroundActivity::TwoAndHalfMinutes;
        break;
    case 2:
        freq = BackgroundActivity::FiveMinutes;
        break;
    case 3:
        freq = BackgroundActivity::TenMinutes;
        break;
    case 4:
        freq = BackgroundActivity::FifteenMinutes;
        break;
    case 5:
        freq = BackgroundActivity::ThirtyMinutes;
        break;
    case 6:
        freq = BackgroundActivity::OneHour;
        break;
    case 7:
        freq = BackgroundActivity::TwoHours;
        break;
    case 8:
        freq = BackgroundActivity::FourHours;
        break;
    case 9:
        freq = BackgroundActivity::EightHours;
        break;
    case 10:
        freq = BackgroundActivity::TenHours;
        break;
    case 11:
        freq = BackgroundActivity::TwelveHours;
        break;
    case 12:
        freq = BackgroundActivity::TwentyFourHours;
        break;
    }

    qDebug() << "Changing notification frequency to " << freq;
    activity->setWakeupFrequency(freq);

    if (!m_disabled) {
        activity->wait(freq);
    }
}

void Notifications::disable() {
    qDebug() << "Disabling notifications";
    if (!m_disabled) {
        m_disabled = true;
        activity->stop();
    }

}

void Notifications::enable() {
    if (m_disabled) {
        m_disabled = false;
        start_time = QDateTime::currentMSecsSinceEpoch();
        activity->setWakeupFrequency(freq);
        activity->wait(freq);
    }
}

void Notifications::startRun() {
    qDebug() << "Starting run";
    if (m_disabled) {
        return;
    }

    if (!manager->networkAccessible()) {
        qDebug() << "Waiting due to no network access";
        activity->wait(freq);
        return;
    }

    {
        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/matrix-rooms.cache.json");
        QDateTime cache_file_time = QFileInfo(cache_file).lastModified();
        if (rooms_cache.isNull() || (cache_file_time.isValid() && cache_file_time > rooms_cache_time)) {
            if (cache_file.open(QFile::ReadOnly)) {
                rooms_cache = QJsonDocument::fromJson(cache_file.readAll());
            }
            rooms_cache_time = cache_file_time;
        }
    }

    {
        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/org.yeheng/sailtrix/users.json");
        QDateTime cache_file_time = QFileInfo(cache_file).lastModified();
        if (users_cache.isNull() || (cache_file_time.isValid() && cache_file_time > users_cache_time)) {
            if (cache_file.open(QFile::ReadOnly)) {
                users_cache = QJsonDocument::fromJson(cache_file.readAll());
            }
            users_cache_time = cache_file_time;
        }
    }

    QByteArray secret_stored = get_secret(&m_secretManager, config_secret_id());
    if (secret_stored != nullptr) {
        QJsonDocument document = QJsonDocument::fromJson(secret_stored);
        m_access_token = document.object().value("access_token").toString();
        m_hs_url = document.object().value("home_server").toString();
    } else {
        qWarning() << "Cannot get secret for notifications";
        return;
    }

    qDebug() << "Polling for notifications";
    QString url(m_hs_url + "/_matrix/client/r0/notifications?");

    if (next.isEmpty()) {
        url.append("limit=2");
    } else {
        url.append("limit=4&from=" + next);
        qDebug() << "From:" << next;
    }

    QNetworkRequest req(url);
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + m_access_token).toUtf8()));

    manager->get(req);
}

void Notifications::processNotifications(QNetworkReply* reply) {
    reply->deleteLater();

    if (m_disabled) {
        return;
    }

    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "Get notifications failed:" << reply->error() << reply->errorString() << reply->readAll();
    } else {
        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        qDebug() << "Received notifications object:" << doc;
        QJsonObject notif_obj = doc.object();
        QJsonArray notifications = notif_obj.value("notifications").toArray();

        qint64 notif_time = start_time;

        for (QJsonValue notif : notifications) {
            QJsonObject this_notif = notif.toObject();

            if (this_notif.value("actions").toArray().contains("notify")
                || this_notif.value("actions").toArray().contains("coalesce")
                || this_notif.value("actions").toString() == QStringLiteral("notify")
                || this_notif.value("actions").toString() == QStringLiteral("coalesce")) {

                QString room_id = this_notif.value("room_id").toString();
                QJsonObject event = this_notif.value("event").toObject();

                notif_time = this_notif.value("ts").toVariant().toULongLong();
                if (notif_time > start_time) {
                    qDebug() << "Received notification:" << event.value("content").toObject();

                    Notification* n;

                    QMap<QString, Notification*>::iterator iter = notifications_map.find(room_id);
                    if (iter != notifications_map.end()) {
                        n = iter.value();
                        n->setItemCount(n->itemCount() + 1);
                    } else {
                        n = new Notification;
                        n->setAppIcon("harbour-sailtrix");
                        n->setAppName("Sailtrix");
                        n->setCategory("x-nemo.messaging.im");
                        n->setHintValue(QStringLiteral("x-nemo-priority"), 120);
                        n->setHintValue(QStringLiteral("x-nemo-feedback"), QStringLiteral("chat_exists"));
                        QString summary = "New Message";
                        QString icon = "image://theme/icon-lock-chat";

                        if (!rooms_cache.isNull()) {
                            QString iconPath = rooms_cache.object().value("rooms_list").toObject().value(room_id).toObject().value("avatar").toString();
                            if (iconPath.startsWith("/")) {
                                icon = iconPath;
                            }

                            summary = rooms_cache.object().value("rooms_list")
                                .toObject()
                                .value(room_id).toObject().value("name").toString();
                        }

                        n->setIcon(icon);
                        n->setSummary(summary);
                        n->setItemCount(1);

                        QVariantList args;
                        args.append(this_notif.value("room_id").toString());

                        QVariantList actions;
                        actions.append(Notification::remoteAction(QStringLiteral("default"), QStringLiteral("showRoom"), QStringLiteral("org.yeheng.sailtrix"), QStringLiteral("/org/yeheng/sailtrix"), QStringLiteral("org.yeheng.sailtrix"), QStringLiteral("showRoom"), args));
                        n->setRemoteActions(actions);

                        n->setUrgency(Notification::Normal);

                        notifications_map.insert(room_id, n);
                    }

                    if (new_notifications.find(n) == new_notifications.end())
                    {
                        QString sender = event.value("sender").toString();
                        if (!users_cache.isNull()) {
                            QJsonValue senderValue = users_cache.object().value(sender);
                            if (senderValue.isObject()) {
                                sender = senderValue.toObject().value("display_name").toString();
                            }
                        }

                        QString body;
                        if (event.value("type").toString() == QStringLiteral("m.room.encrypted")) {
                            body = "Encrypted message";
                        } else {
                            body = event.value("content").toObject().value("body").toString();
                        }

                        body = sender + ": " + body;

                        n->setBody(body);
                        n->setPreviewBody(body);
                        n->setTimestamp(QDateTime::fromMSecsSinceEpoch(this_notif.value("ts").toVariant().toLongLong()));
                        new_notifications.insert(n);
                    }
                }
            }
        }

        next = notif_obj.value("next_token").toString();
        if (notif_time > start_time && !next.isEmpty()) {
            startRun();
            return;
        }
    }

    for (Notification* n : new_notifications) {
        qDebug() << "Publishing Notification: " << n->body() << "With ID OF " << n->replacesId();
        n->publish();
    }

    new_notifications.clear();
    next.clear();

    start_time = QDateTime::currentMSecsSinceEpoch();
    activity->wait(freq);

    qDebug() << "waiting activity for next run";
}

void Notifications::pause() {
    if (m_disabled) {
        return;
    }

    activity->stop();

    for (auto iter = notifications_map.begin(); iter != notifications_map.end(); ++iter) {
        iter.value()->close();
        delete iter.value();
    }
    notifications_map.clear();
    new_notifications.clear();
}

void Notifications::resume() {
    if (m_disabled) {
        return;
    }

    start_time = QDateTime::currentMSecsSinceEpoch();
    activity->wait(freq);
}

bool Notifications::isStopped() {
    if (m_disabled) {
        return true;
    }
    return activity->isStopped();
}
